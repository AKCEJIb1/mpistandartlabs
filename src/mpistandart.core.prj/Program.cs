﻿using MPIStandart.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPIStandart.Core
{
    class Program
    {
        static void Main(string[] args)
        {

            var rand = new Random();

            var matrix = Enumerable.Range(0, 3)
                .Select(r =>
                    Enumerable.Range(0, 3)
                    .Select(x => (double)rand.Next(-10, 10))
                    .ToArray())
                .ToArray();

            var vector = Enumerable.Range(0, 3)
                .Select(x => (double)rand.Next(-10, 10))
                .ToArray();


            switch (args[0])
            {
                case "matrix":
                    MatrixCalc.Process(matrix, vector);
                    break;
                case "search":
                    if (int.TryParse(args[1], out var count))
                    {
                       Search.Process(count, args[2]);
                    }
                    break;
            }

        }
    }
}
