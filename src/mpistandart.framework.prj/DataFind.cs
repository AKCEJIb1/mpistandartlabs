﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPIStandart.Framework
{
    [Serializable]
    public class Person
    {
        public Person(
            string firstname,
            string surname,
            int age)
        {
            Firstname = firstname;
            Surname = surname;
            Age = age;
        }

        public static Person Empty = new Person("NONE", "NONE", 0);

        public string Firstname { get; }
        public string Surname { get; }
        public int Age { get; }

        public override string ToString()
        {
            return $"Person [Firstname = {Firstname}, Surname = {Surname}, Age = {Age}]";
        }

        public Person(Person person)
        {
            Firstname = person.Firstname;
            Surname = person.Surname;
            Age = person.Age;
        }

        public override bool Equals(object obj)
        {
            var p = (Person)obj;
            return Firstname.Equals(p.Firstname)
                && Surname.Equals(p.Surname)
                && Age.Equals(p.Age);
        }

        public override int GetHashCode()
        {
            return Firstname[0] ^ Surname[0];
        }
    }
    public static class Search
    {
        public static void Process(int count, string surname)
        {


            MPI.Environment.Run(comm =>
            {
                Person[] people = new Person[count];
                int[] sizes = new int[comm.Size];

                if (comm.Rank == 0)
                {
                    var peopleArr = Enumerable.Range(0, count)
                        .Select((x, i) => new Person($"Имя{i}", $"Фамилия{i}", 10 + i))
                        .ToArray();

                    var procs = comm.Size;
                    var len = peopleArr.Length;
                    var szList = new List<int>();

                    if (procs > len)
                    {
                        Console.WriteLine("Слишком много процессов!");
                        comm.Abort(-1);
                    }

                    // Считаем кому сколько кусочков дать
                    while (len > 0)
                    {
                        var chunkSize = (int)Math.Ceiling(len / (double)procs--);
                        len -= chunkSize;

                        szList.Add(chunkSize);
                    }

                    people = peopleArr;
                    sizes = szList.ToArray();
                }

                // Раздаём всем кусочки
                var currentPeoples = comm.ScatterFromFlattened(people, sizes, 0);

                Console.WriteLine($"Ранг процесса: {comm.Rank}, Количество записей: {currentPeoples.Length}");

                // Синхрофазатрон? (етачё)
                comm.Barrier();

                if(comm.Rank == 0)
                    Console.WriteLine("Все процессы были инициализированы! Начинаем поиск.");

                Person foundPerson = Person.Empty;
                var found = false;

                foreach (var item in currentPeoples)
                {
                    var status = comm.ImmediateProbe(MPI.Communicator.anySource, 1);

                    if(status != null)
                    {
                        var isFound = comm.Receive<bool>(MPI.Communicator.anySource, 1);
                        if (isFound)
                        {
                            Console.WriteLine($"Останавливаем поиск в процессе с рангом = {comm.Rank}");
                            break;
                        }
                    }

                    if (item.Surname.Equals(surname))
                    {
                        Console.WriteLine($"Объект был найден процессом с рангом = {comm.Rank}");

                        foundPerson = item;
                        found = true;
                        if(comm.Rank > 0)
                            comm.Send(foundPerson, 0, 0);

                        break;
                    }
                    // Небольшая имитация задержки...
                    //System.Threading.Thread.Sleep(2);
                }
                Console.WriteLine($"Процесс {comm.Rank} закончил работу!");
                // Оповещаем остальные процессы о том что мы закончили работу!
                for (int i = 0; i < comm.Size; i++)
                {
                    if (i != comm.Rank)
                        comm.Send(found, i, 1);
                }

                // Опять синхрофазатрон? Зашто?!
                comm.Barrier();

                if (comm.Rank == 0)
                {
                    Console.WriteLine("\n--------РЕЗУЛЬТАТ--------");


                    var result = comm.ImmediateProbe(MPI.Communicator.anySource, 0);
                    if (result != null)
                    {
                        foundPerson = comm.Receive<Person>(MPI.Communicator.anySource, 0);
                    }

                    if (!foundPerson.Equals(Person.Empty))
                    {
                        Console.WriteLine(foundPerson);
                    }else
                        Console.WriteLine("НИЧЕГО НЕ НАЙДЕНО!");
                    
                }


            });
        }
    }
}
