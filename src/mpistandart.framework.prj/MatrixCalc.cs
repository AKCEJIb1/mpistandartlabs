﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MPIStandart.Framework
{
    public static class MatrixCalc
    {

        private static double Dot(double[] a, double[] b)
        {
            var sum = 0d;
            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i] * b[i];
            }
            return sum;
        }

        public static void Process(double[][] matrix, double[] vector)
        {

            MPI.Environment.Run(communicator =>
            {
                var currentRow = communicator.Scatter(matrix, 0);
                var resultVector = communicator.Allgather(Dot(currentRow, vector));

                if(communicator.Rank == 0)
                {

                    Console.WriteLine("Matrix: ");
                    for (int i = 0; i < matrix.Length; i++)
                    {
                        for (int j = 0; j < matrix[i].Length; j++)
                        {
                            Console.Write($"{matrix[i][j]} ");
                        }
                        Console.WriteLine();
                    }

                    Console.WriteLine("Vector: ");

                    for (int i = 0; i < vector.Length; i++)
                    {
                        Console.Write($"{vector[i]} ");
                    }

                    Console.WriteLine();

                    var data = "";

                    for (int i = 0; i < resultVector.Length; i++)
                    {
                        data += $"{resultVector[i]} ";
                    }

                    Console.WriteLine($"Result: {data}");
                }
            });

        }
    }
}
